import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
	{
		path: 'login',
		loadChildren: () => import('@app/users').then(m => {/** */ console.log('@app/users'); return m.LoginModule;}),
	},
	{
		path: 'profile',
		loadChildren: () => import('@app/profiles').then(m => {/** */ console.log('@app/profiles'); return m.ViewProfileModule; }),
	},
	{
		path: '',
		redirectTo: 'profile',
		pathMatch: 'full',
	},
	{
		path: '**',
		redirectTo: 'profile',
		pathMatch: 'full',
	},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
