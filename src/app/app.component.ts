import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AppStore } from './app.store';

@Component({
  selector: 'app-root',
  templateUrl: `./app.component.html`,
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  view = {
    title: 'Simple Auth',
    isLoggedIn: new Observable<boolean>(),
  };
  constructor(
    private appStore: AppStore
  ) {}

  ngOnInit() {
    this.view.isLoggedIn = this.appStore.isLoggedIn;
  }

  logout(): void {
    this.appStore.logout();
  }
}
