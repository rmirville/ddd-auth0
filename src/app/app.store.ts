import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { SubjAppStore } from "./subj-app.store";

@Injectable({
	providedIn: 'root',
	useClass: SubjAppStore,
})
export abstract class AppStore {
	abstract get isLoggedIn(): Observable<boolean>;
	abstract logout(): void;
}
