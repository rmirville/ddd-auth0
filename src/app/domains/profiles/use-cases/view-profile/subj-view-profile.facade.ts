import { Injectable } from "@angular/core";
import { BehaviorSubject, EMPTY, map, Observable } from "rxjs";
import { ViewProfileFacade } from "./view-profile.facade";

interface ViewProfilesState {
	name: string;
	pronouns: string;
	color: string;
}

const initialState: ViewProfilesState = {
	name: '',
	pronouns: '',
	color: '',
};

@Injectable()
export class SubjViewProfileFacade implements ViewProfileFacade {
	private store: BehaviorSubject<ViewProfilesState> = new BehaviorSubject<ViewProfilesState>(initialState);
	private state: Observable<ViewProfilesState> = this.store.asObservable();
	private name$: Observable<string> = this.state.pipe(map(s => s.name));
	private pronouns$: Observable<string> = this.state.pipe(map(s => s.pronouns));
	private color$: Observable<string> = this.state.pipe(map(s => s.color));
	
	constructor() {

	}

	get name(): Observable<string> {
		return this.name$;
	}
	
	get pronouns(): Observable<string> {
		return this.pronouns$;
	}
	
	get color(): Observable<string> {
		return this.color$;
	}
}
