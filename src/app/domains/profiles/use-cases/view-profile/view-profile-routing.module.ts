import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ViewProfileComponent } from "./components/view-profile/view-profile.component";
import { UsersGuard } from "@app/users/guards/users.guard";

const routes: Routes = [
	{
		path: '',
		component: ViewProfileComponent,
		canActivate: [ UsersGuard ],
	}
];

@NgModule({
	imports: [
		RouterModule.forChild(routes),
	]
})
export class ViewProfileRoutingModule {}
