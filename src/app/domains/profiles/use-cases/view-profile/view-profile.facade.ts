import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ProfilesStoreModule } from "@app/profiles";
import { SubjViewProfileFacade } from "./subj-view-profile.facade";

@Injectable({
	providedIn: ProfilesStoreModule,
	useClass: SubjViewProfileFacade,
})
export abstract class ViewProfileFacade {
	abstract get name(): Observable<string>;
	abstract get pronouns(): Observable<string>;
	abstract get color(): Observable<string>;
}
