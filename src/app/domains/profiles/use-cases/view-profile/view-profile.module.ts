import { NgModule } from "@angular/core";
import { ViewProfileComponent } from "./components/view-profile/view-profile.component";
import { ViewProfileRoutingModule } from "./view-profile-routing.module";

@NgModule({
	imports: [ ViewProfileRoutingModule ],
	declarations: [ ViewProfileComponent ],
	exports: [ ViewProfileComponent ],
})
export class ViewProfileModule {}
