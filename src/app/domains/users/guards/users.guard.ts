import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterModule} from '@angular/router';
import { UsersModule } from "@app/users/users.module";
import { AuthGuard } from "@auth0/auth0-angular";

@Injectable({
	providedIn: UsersModule,
	useExisting: AuthGuard,
	deps: [ RouterModule ]
})
export abstract class UsersGuard implements CanActivate {
	abstract canActivate(route: ActivatedRouteSnapshot): boolean;
}
