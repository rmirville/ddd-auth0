import { Injectable } from "@angular/core";
import { AuthService } from "@auth0/auth0-angular";
import { BehaviorSubject, distinctUntilChanged, Observable } from "rxjs";
import { LoggedInService } from "./logged-in.service";

@Injectable()
export class Auth0LoggedInService implements LoggedInService {
	private store: BehaviorSubject<boolean> = new BehaviorSubject(false);
	private isLoggedIn$: Observable<boolean> = this.store.asObservable();
	constructor(private auth: AuthService) {
		this.auth.isAuthenticated$.pipe(distinctUntilChanged()).subscribe(isAuthenticated => {
			this.store.next(isAuthenticated);
		})
	}
	public get isLoggedIn(): Observable<boolean> {
		return this.isLoggedIn$;
	}
}