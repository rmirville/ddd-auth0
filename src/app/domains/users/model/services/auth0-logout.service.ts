import { DOCUMENT } from "@angular/common";
import { Inject, Injectable } from "@angular/core";
import { AuthService } from "@auth0/auth0-angular";
import { LogoutService } from "./logout.service";
import { environment } from "src/environments/environment";

@Injectable()
export class Auth0LogoutService implements LogoutService {
	constructor(
		@Inject(DOCUMENT) public document: Document,
		private auth: AuthService,
	) {	}
	logout(): void {
		this.auth.logout({ returnTo: environment.auth.redirectUri });
	}
}
