import { Injectable } from "@angular/core";
import { UsersModule } from "@app/users";
import { Observable } from "rxjs";
import { Auth0LoggedInService } from "./auth0-logged-in.service";

@Injectable({
	providedIn: UsersModule,
	useClass: Auth0LoggedInService,
})
export abstract class LoggedInService {
	abstract get isLoggedIn(): Observable<boolean>;
}
