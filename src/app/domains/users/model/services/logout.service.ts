import { Injectable } from "@angular/core";
import { UsersModule } from "@app/users";
import { Auth0LogoutService } from "./auth0-logout.service";

@Injectable({
	providedIn: UsersModule,
	useClass: Auth0LogoutService,
})
export abstract class LogoutService {
	abstract logout(): void;
}
