import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { UsersModule } from "@app/users/users.module";
import { SubjLoginFacade } from "@app/users/store/subj-login.facade";
import { AuthModule } from "@auth0/auth0-angular";

@Injectable({
	providedIn: UsersModule,
	useClass: SubjLoginFacade,
	deps: [ AuthModule ],
})
export abstract class LoginFacade {
	abstract login(): void;
	abstract isLoggedIn(): Observable<boolean>;
}
