import { Injectable } from '@angular/core';
import { AuthService } from "@auth0/auth0-angular";
import { LoginFacade } from './login.facade';
import { BehaviorSubject, from, Observable } from "rxjs";
import {catchError, map} from "rxjs/operators";

interface LoginState {
	isLoggedIn: boolean;
}

const initialState: LoginState = {
	isLoggedIn: false,
};

@Injectable()
export class SubjLoginFacade implements LoginFacade {
	private store: BehaviorSubject<LoginState> = new BehaviorSubject<LoginState>(initialState);
	private state: Observable<LoginState> = this.store.asObservable();
	private isLoggedIn$: Observable<boolean> = this.state.pipe(map(state => state.isLoggedIn));
	constructor(private auth: AuthService) {
	}
	login(): void {
		this.auth.loginWithRedirect().pipe(
			catchError(e => from([null])),
		).subscribe(_ => {
			/** */ console.log('returned from loginWithRedirect()');
			const state: LoginState = this.store.getValue();
			this.store.next({ ...state, isLoggedIn: true });
		});
	}
	isLoggedIn(): Observable<boolean> {
		return this.isLoggedIn$;
	}
}
