import { Component, OnInit } from '@angular/core';
import { SubjLoginFacade } from '@app/users/store/subj-login.facade';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private domainSvc: SubjLoginFacade) { }

  ngOnInit(): void {
  }

	onClickLogin() {
		this.domainSvc.login();
	}

}
