import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { LoginComponent } from "./components/login/login.component";
import { LoginRoutingModule } from "./login-routing.module";
import { UsersModule } from "@app/users/users.module";

@NgModule({
  declarations: [
    LoginComponent
  ],
  imports: [
    CommonModule,
		LoginRoutingModule,
		UsersModule,
  ]
})
export class LoginModule { }
