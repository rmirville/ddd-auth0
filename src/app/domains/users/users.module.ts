import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SubjLoginFacade } from './store/subj-login.facade';



@NgModule({
  imports: [
    CommonModule
  ],
	providers: [
		SubjLoginFacade
	],
})
export class UsersModule { }
