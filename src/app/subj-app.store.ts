import { Injectable } from "@angular/core";
import { LoggedInService, LogoutService } from "@app/users";
import { BehaviorSubject, map, Observable } from "rxjs";
import { AppStore } from "./app.store";

interface AppState {
	isLoggedIn: boolean;
}

const initialState: AppState = {
	isLoggedIn: false,
};

@Injectable()
export class SubjAppStore implements AppStore {
	private readonly store: BehaviorSubject<AppState> = new BehaviorSubject(initialState);
	private readonly state: Observable<AppState> = this.store.asObservable();
	private readonly isLoggedIn$: Observable<boolean> = this.state.pipe(map(s => s.isLoggedIn));
	constructor(
		private loggedInSvc: LoggedInService,
		private logoutSvc: LogoutService,
	) {
		this.loggedInSvc.isLoggedIn.subscribe(isLoggedIn => {
			this.store.next({ ...this.store.value, isLoggedIn });
		});
	}
	public get isLoggedIn(): Observable<boolean> {
		return this.isLoggedIn$;
	}
	public logout(): void {
		this.logoutSvc.logout();
	}
}
